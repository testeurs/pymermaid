
<div style="display:none;">
<!-- 
Cette div n'est là juste pour initialiser mermaid mais n'apparaît pas. Pas très propre, cela doit pouvoir se faire en JS pur avec un genre de :
$(document).ready(function () {
  mermaid.initialize();
}); 
-->
```mermaid
graph TD
    A <--> B
```
</div>

Essai 

{{ IDE('graphe') }}

<div id="cible" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
Votre graphe sera ici
</div>
