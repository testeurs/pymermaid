import js

global truc

async def dessine(sommets, nom_div="cible"):

    div = js.document.getElementById(nom_div)

    graph_def = "flowchart LR\n    "
    graph_def += " --> ".join(s for s in sommets)

    graph = await js.mermaid.render(f"c", graph_def)
    
    truc = "<center>" + graph.svg + "</svg>"
    
    div.innerHTML = truc
    
    
dessine(["Ceci", "est", "un", "graphe", "!"])
