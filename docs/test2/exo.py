import js

from random import randrange

def vers_mermaid():
    return f"""graph TB
    N("{randrange(100)}")
    N0("{randrange(100)}")
    N --- N0
    N1(" ")
    N --- N1
    N00("{randrange(100)}")
    N0 --- N00
    N01("47")
    N0 --- N01
    N000(" ")
    N00 --- N000
    N00 --- N001
    N01 --- N010
    N01 --- N011
    N001(" ")
    N010(" ")
    N011(" ")
"""


début = '&ltpre class="mermaid" id="trucunique">'
fin = '&lt/pre>'
elt_id = "trucunique"
elt = js.document.getElementById("fig_mermaid")
elt.innerHTML = (début + vers_mermaid() + fin).replace("&lt", "<")
js.mermaid.init()
