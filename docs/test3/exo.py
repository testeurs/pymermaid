import js

from random import randrange

def vers_mermaid():
    return f"""graph TB
    N("{randrange(100)}")
    N0("{randrange(100)}")
    N --- N0
    N1(" ")
    N --- N1
    N00("{randrange(100)}")
    N0 --- N00
    N01("47")
    N0 --- N01
    N000(" ")
    N00 --- N000
    N00 --- N001
    N01 --- N010
    N01 --- N011
    N001(" ")
    N010(" ")
    N011(" ")
"""


def dessine():

    élément = js.document.getElementById("fig_mermaid")
    print("ok 1")
    élément.innerHTML = vers_mermaid()
    print("ok 2")
    élément.setAttribute("class", "mermaid")
    print("ok 3")
    élément.removeAttribute("data-processed")
    print("ok 4")
    js.mermaid.init()
    print("ok 5")

dessine()
