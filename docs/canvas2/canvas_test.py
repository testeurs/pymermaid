# --- PYODIDE:env --- #
import js
import asyncio

async def bosse():
    N = 100 -1
    M = 100 -1
    import random
    import time
    
    for _ in range(40):
        n = 1
        xs = [4 * random.randrange(N+1)]
        ys = [4 * random.randrange(N+1)]

        js.KVTest(n, xs, ys)
        await time.sleep(0.1)


# --- PYODIDE:code --- #

bosse()
